package main

import (
	"log"
	"net/http"

	"gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/repository"
	"gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/router"
	"gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/services"
	"gitlab.com/moohii/ARGeocoreService/pkg/db"

	cfg "gitlab.com/moohii/ARGeocoreService/pkg/configs"
	pg "gitlab.com/moohii/ARGeocoreService/pkg/db/postgres"
)

type config struct {
	DB   *db.Config `yaml:"db"`
	Addr string     `yaml:"addr"`
}

// @title Swagger ARGeocore API
// @version 1.0
// @description This is an ARGeocore API

// @BasePath /api/v1/0x7a81b3f14a609e2d

// @securityDefinitions.basic  BasicAuth
// @security basicAuth

func main() {
	var conf config
	err := cfg.UnmarshalYAML("/configs/config.yaml", &conf)
	if err != nil {
		log.Fatal(err)
	}

	db, err := pg.NewConnection(conf.DB.URL())
	if err != nil {
		log.Fatal(err)
	}

	repo := repository.NewRepository(db.DB)
	svc := services.NewService(repo)
	router := router.NewRouter(svc)

	log.Printf("Server is starting on port %s", conf.Addr)
	log.Fatal(http.ListenAndServe(conf.Addr, router))
}
