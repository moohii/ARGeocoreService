# ARGeocoreService

## Overview
ARGEocoreService is a RESTful API for managing geo-coordinate data. This service is built with Go, uses PostgreSQL as the database, and Docker for containerization. The API is documented using Swagger.

## Features
- Create new coordinates
- Fetch all coordinates
- Swagger documentation
- Basic Authentication

## Prerequisites
- [Docker](https://www.docker.com/products/docker-desktop)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Go](https://golang.org/dl/) (for local development)
- [Migrate](https://github.com/golang-migrate/migrate) (for database migrations)

### Makefile Targets
- **service-up**: Build and start the ARGeocoreService.
- **db-up**: Start the PostgreSQL database.
- **migrations-up**: Run database migrations.
- **migrations-down**: Rollback all database migrations.
- **up**: Start the PostgreSQL database, wait until it's ready, run migrations, and then start the ARGeocoreService.

## Docker Setup
The project uses Docker for containerization. Make sure Docker and Docker Compose are installed on your system.

### Running with Docker Compose
To start the service and the PostgreSQL database with Docker Compose, run:
```sh
make up
```
## License
This project is licensed under the Apache License 2.0.
