DIR=${CURDIR}

service-up:
	docker compose up --build argeocore

db-up:
	docker compose up -d argeocore-postgres

migrations-up:
	docker run --rm -v $(DIR)/pkg/ar_geocore/repository/postgres/migrations:/migrations \
		--network argeocoreservice_default migrate/migrate -path=/migrations/ \
		-database postgres://dev:12345@argeocore-postgres:5432/argeocore?sslmode=disable up

migrations-down:
	docker run --rm -v $(DIR)/pkg/ar_geocore/repository/postgres/migrations:/migrations \
		--network argeocoreservice_default migrate/migrate -path=/migrations/ \
		-database postgres://dev:12345@argeocore-postgres:5432/argeocore?sslmode=disable down -all

up:
	chmod +x ./hack/scripts/wait-for-postgres.sh
	docker compose up -d argeocore-postgres
	./hack/scripts/wait-for-postgres.sh argeocore-postgres
	$(MAKE) migrations-up
	docker compose up -d argeocore