package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/moohii/ARGeocoreService/pkg/models"
)

func respondJSON(w http.ResponseWriter, code int, response any) {
	b, err := json.Marshal(response)
	if err != nil {
		http.Error(
			w,
			http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError,
		)
		return
	}
	fmt.Println("alalalalalalalalala")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(b)
}

func respondError(w http.ResponseWriter, code int, err error) {
	respondJSON(w, code, &models.Error{
		Code:    code,
		Message: err.Error(),
	})
}