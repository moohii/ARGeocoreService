package handlers

import "gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/services"

type Handler struct {
	svc *services.Service
}

func NewHandler(svc *services.Service) *Handler {
	return &Handler{svc: svc}
}