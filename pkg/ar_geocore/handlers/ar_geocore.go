package handlers

import (
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/moohii/ARGeocoreService/pkg/models"
)

// CreateCoordinates godoc
// @Summary Create new coordinates
// @Description Create new coordinates in the database (temporarily indicate id 1 in request body if you want to add new coordinates. it necessary to overwrite one row which was created earlier)
// @Tags coordinates
// @Accept json
// @Produce json
// @Param coordinates body []models.Geojson true "Array of Geojson objects"
// @Success 200 {array} models.Geojson
// @Failure 400 {object} models.Error
// @Failure 500 {object} models.Error
// @Security BasicAuth
// @Router /api/v1/0x7a81b3f14a609e2d/coordinates [post]
func (h *Handler) CreateCoordinates() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			respondError(w, http.StatusBadRequest, err)
			return
		}
		defer r.Body.Close()

		var geojsons []*models.Geojson
		err = json.Unmarshal(b, &geojsons)
		if err != nil {
			respondError(w, http.StatusBadRequest, err)
			return
		}

		err = h.svc.ARGeocore.CreateCoordinates(r.Context(), geojsons)
		if err != nil {
			respondError(w, http.StatusInternalServerError, err)
			return
		}

		respondJSON(w, http.StatusOK, geojsons)
	}
}

// FetchCoordinates godoc
// @Summary Fetch coordinates
// @Description Get all coordinates from the database (temporarily db can keep only one row with coordinates)
// @Tags coordinates
// @Produce json
// @Success 200 {array} models.Geojson
// @Failure 404 {object} models.Error
// @Failure 500 {object} models.Error
// @Security BasicAuth
// @Router /api/v1/0x7a81b3f14a609e2d/coordinates [get]
func (h *Handler) FetchCoordinates() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		coordinates, err := h.svc.ARGeocore.FetchCoordinates(r.Context())
		if err != nil {
			respondError(w, http.StatusInternalServerError, err)
			return
		}

		respondJSON(w, http.StatusOK, coordinates)
	}
}
