package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/handlers"
	"gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/services"

	httpSwagger "github.com/swaggo/http-swagger/v2"
	_ "gitlab.com/moohii/ARGeocoreService/docs"
)

func NewRouter(svc *services.Service) *mux.Router {
	handler := handlers.NewHandler(svc)

	r := mux.NewRouter()

	apiV1 := r.PathPrefix("/api/v1/0x7a81b3f14a609e2d").Subrouter()

	apiV1.PathPrefix("/swagger/*").Handler(httpSwagger.Handler(
		httpSwagger.URL("http://localhost:3307/swagger/doc.json"),
	)).Methods(http.MethodGet)
	apiV1.HandleFunc("/coordinates", handler.FetchCoordinates()).Methods(http.MethodGet)
	apiV1.HandleFunc("/coordinates", handler.CreateCoordinates()).Methods(http.MethodPost)

	return r
}
