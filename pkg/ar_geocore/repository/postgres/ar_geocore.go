package postgres

import (
	"context"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"gitlab.com/moohii/ARGeocoreService/pkg/models"
)

type ARGeocoreRepo struct {
	db *sqlx.DB
}

func NewARGeocoreRepo(db *sqlx.DB) *ARGeocoreRepo {
	return &ARGeocoreRepo{db: db}
}

func (repo *ARGeocoreRepo) CreateCoordinates(ctx context.Context, coordinates []*models.Geojson) error {
	insertQuery := squirrel.Insert("coordinates").Columns("id", "coordinates")

	for _, coocoordinate := range coordinates {
		insertQuery = insertQuery.Values(
			coocoordinate.ID,
			coocoordinate.Coordinates,
		)
	}

	query, args, err := insertQuery.Suffix("ON CONFLICT (id) DO UPDATE SET coordinates = EXCLUDED.coordinates RETURNING id").PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return err
	}
	rows, err := repo.db.QueryxContext(ctx, query, args...)
	if err != nil {
		return err
	}

	for i := 0; rows.Next(); i++ {
		rows.Scan(&coordinates[i].ID)
	}

	return nil
}

func (repo *ARGeocoreRepo) FetchCoordinates(ctx context.Context) ([]*models.Geojson, error) {
	query, args, err := squirrel.Select("*").From("coordinates").PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return nil, fmt.Errorf("something went wrong while forming query, err: %v", err)
	}
	
	geojsons := []*models.Geojson{}
	err = sqlx.SelectContext(ctx, repo.db, &geojsons, query, args...)
	if err != nil {
		return nil, fmt.Errorf("something went wrong while execution query, err: %v", err)
	}

	return geojsons, nil
}