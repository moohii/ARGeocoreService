CREATE TABLE coordinates (
    id BIGINT NOT NULL PRIMARY KEY,
    coordinates jsonb NOT NULL UNIQUE
);