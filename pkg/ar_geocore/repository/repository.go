package repository

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/repository/postgres"
	"gitlab.com/moohii/ARGeocoreService/pkg/models"
)

type ARGeocore interface{
	CreateCoordinates(ctx context.Context, coordinates []*models.Geojson) error
	FetchCoordinates(ctx context.Context) ([]*models.Geojson, error)
}

type Repository struct {
	ARGeocore ARGeocore
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		ARGeocore: postgres.NewARGeocoreRepo(db),
	}
}