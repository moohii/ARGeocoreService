package services

import (
	"context"

	"gitlab.com/moohii/ARGeocoreService/pkg/models"
	"gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/repository"
)

type ARGeocoreService struct {
	repo repository.Repository
}

func NewARGeocoreService(repo repository.Repository) *ARGeocoreService {
	return &ARGeocoreService{repo: repo}
}

func (svc *ARGeocoreService) FetchCoordinates(ctx context.Context) ([]*models.Geojson, error) {
	return svc.repo.ARGeocore.FetchCoordinates(ctx)
}

func (svc *ARGeocoreService) CreateCoordinates(ctx context.Context, coordinates []*models.Geojson) error {
	return svc.repo.ARGeocore.CreateCoordinates(ctx, coordinates)
}