package services

import (
	"context"

	"gitlab.com/moohii/ARGeocoreService/pkg/models"
	"gitlab.com/moohii/ARGeocoreService/pkg/ar_geocore/repository"
)

type ARGeocore interface{
	CreateCoordinates(ctx context.Context, coordinates []*models.Geojson) error
	FetchCoordinates(ctx context.Context) ([]*models.Geojson, error)
}

type Service struct {
	ARGeocore ARGeocore
}

func NewService(repo *repository.Repository) *Service {
	return &Service{
		ARGeocore: repo.ARGeocore,
	}
}