package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"

	_ "github.com/jackc/pgx/v5/stdlib" 
)

type DB struct {
	*sqlx.DB
}

func NewConnection(connectionString string) (*DB, error) {
	
	db, err := sqlx.Connect("pgx", connectionString)
	if err != nil {
		return nil, fmt.Errorf("something went wrong during db connection, err: %v", err)
	}

	return &DB{
		DB: db,
	}, nil
}