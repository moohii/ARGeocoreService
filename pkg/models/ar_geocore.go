package models

import "encoding/json"

// swagger:model Geojson
type Geojson struct {
	ID          uint64          `json:"id,omitempty" db:"id" `
	Coordinates json.RawMessage `json:"coordinates" db:"coordinates" `
}
