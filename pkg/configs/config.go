package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

// UnmarshalYAML unmarshals a .yaml file at path to destination dest
func UnmarshalYAML(path string, dest any) error {
	b, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("something went wrong during reading config, err: %v", err)
	}

	return yaml.Unmarshal(b, dest)
}